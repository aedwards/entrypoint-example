plugins {
    kotlin("multiplatform") version "1.5.31"
}

repositories {
    mavenLocal()
    mavenCentral()
}

version = "0.0.1"

kotlin {
    js(IR) {
        useCommonJs()
        nodejs()
        binaries.executable()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.2.1")
            }
        }
        val jsMain by getting {
            dependencies { }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}
